import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TestSystemOut {
 
    /**
     * main.
     * 
     * @param args
     */
    public static void main(String[] args) {
    	 System.out.print("old String!");
        ByteArrayOutputStream baoStream = new ByteArrayOutputStream(1024);
        // cache stream
        PrintStream cacheStream = new PrintStream(baoStream);
        // old stream
        PrintStream oldStream = System.out;
 
        System.setOut(cacheStream);
 
        System.out.print("hello world!");
 
        String message = baoStream.toString();
 
        message = "<-- " + message + " -->";
 
        // Restore old stream
        System.setOut(oldStream);
 
        System.out.println(message);
    }
}