import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;

public class SysoutMethodInvokeTest {
    public static void main(String[] args) throws Exception {
    	Sysout sysout=new Sysout();
	    Method mainMethod = sysout.getClass().getMethod("main",String[].class);
 
	   //触发main方法
	    mainMethod.invoke(null, (Object) new String[] { });
   }
}
