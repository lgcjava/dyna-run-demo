package org.jui.core.dynamic;

import java.lang.reflect.Method;

public class MainReflect {
	public static void main(String[] args) throws Exception {
        String className = "com.evan.exercise.Test";
        Class clazz = Class.forName(className);
        Method mainMethod = clazz.getMethod("main",
                String[].class);
         
        Method variableMethod = clazz.getMethod("test");
         
        Method staticMethod = clazz.getMethod("staticTest");
         
        Method paraMethod = clazz.getMethod("parameterTest", String.class,int.class);
        //通过反射调用main方法
        mainMethod.invoke(null, (Object) new String[] { "evan" });
        //通过反射调用无参的成员方法
        variableMethod.invoke(Class.forName(className).newInstance());
        //通过反射调用静态方法
        staticMethod.invoke(clazz.newInstance());
        //通过反射调用有参的成员方法
        paraMethod.invoke(clazz.newInstance(),(Object)new String("evan"),(Object)520);
         
    }
}
